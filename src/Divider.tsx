import { Div_Container1200 } from './components/styles/Container'
import { Header } from './components/Header'
import { NavLink as Link } from 'react-router-dom'
import { Popup } from './components/Popup'
import { colors, size } from './theme'
import { urls } from './urls'
import styled from 'styled-components'

export const Divider = () => {
  return (
    <>
      <Header />
      <Div_Container1200>
        <Main_Wrap>
          <H1_Main>Patrik Ludvik</H1_Main>
          <div>
            <Popup />
            <h1>Hello there!</h1>
            <p>
              My name is Patrik Ludvik and with this website I would like to show you what I am
              working on during my free time. You can find out some sort of info about me and also
              check out my newest projects that I have done.
            </p>
          </div>
        </Main_Wrap>

        <Div_LinkWrap>
          <Div_link to={`/${urls.mainPageJSHistory}`}>
            <P_LinkBodyText>JS History</P_LinkBodyText>
          </Div_link>

          <Div_link to={`/${urls.mainPageCounter}`}>
            <P_LinkBodyText>Counter</P_LinkBodyText>
          </Div_link>

          <Div_link to={`/${urls.mainPageToDoList}`}>
            <P_LinkBodyText>ToDo List</P_LinkBodyText>
          </Div_link>
        </Div_LinkWrap>
      </Div_Container1200>
    </>
  )
}
const H1_Main = styled.h1`
  align-items: center;
  display: flex;
  height: 60px;
  text-decoration: none;
  margin-bottom: 45px;
  display: flex;
`
const Main_Wrap = styled.div`
  align-items: center;
  display: flex;
  margin: auto;
  flex-direction: column;
  font-size: large;
`

const Div_link = styled(Link)`
  color: ${colors.black};
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  display: flex;
  align-items: center;
  background-color: ${colors.header};
  border-radius: 100%;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  margin: 40px 0;
  padding: 40px;
  flex-direction: row;
  cursor: pointer;
  &:hover {
    transform: scale(1.2);
    background: ${colors.react};
  }
`
const P_LinkBodyText = styled.p`
  text-decoration: none;
  &:hover {
    color: ${colors.black};
    transform: scale(1.5);
  }
`
const Div_LinkWrap = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  padding: 30px;
  justify-content: space-evenly;
  text-align: center;
  align-items: center;
  border-radius: 20px;
  margin: 10;
  @media (max-width: ${size.media800}) {
    flex-direction: column;
    width: 100%;
    height: 20%;
    padding-top: 10%;
    top: 0;
    left: 0;
  }
`
