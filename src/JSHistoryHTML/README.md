# Single Web Page

## What is it about ?

_This readme provide you through all functionality of this Single Web Page and you will also find out what was task for HomeWork and what was done_

---

> This Web Page is about JavaScript, you can find out the History of JavaScript, What is JavaScript. Also there are more information like Who introduce JavaScript, what was JavaScript originally called and in the end why was JavaScript created.

> This Web Page offer functionality like send us email, you can choose date when you have time for short meeting and if you have some notes, you can send it along with email.
> Also for more information there are few links and you will be redirect on the another page and if you are really interest in JavaScript, We post some links for video on YouTube for sorting these links on youtube was used a FlexBox, you are able to play it directly on our Web Page but there is also possibility to be redirect on YouTube page.

---

### The Home Work

- [x] Single Web Page
- [x] FlexBox
- [x] MD syntax
- [x] README file
- [x] Fundamentals of JavaScript
- [x] Git, GitLab
- [x] Be ready for another challenge
