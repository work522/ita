import { colors } from '../theme'
import styled from 'styled-components'

export const Button = styled.div`
  color: ${colors.black};
  padding: 15px 32px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
  border: 5px solid black;
  margin: 10px;
  align-items: center;
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  user-select: none;

  &:hover {
    transition: 0.7s;
    font-size: 30px;
    cursor: pointer;
  }
`
