import { Button } from './Button'
import { colors, size } from '../theme'
import React from 'react'
import styled from 'styled-components'

type Props = {}
type State = {
  counter: number
}

export class Counter extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      counter: 0,
    }
  }

  render() {
    return (
      <>
        <Div_Head>
          <Header_Main_H1>Counter App!</Header_Main_H1>
          <StateHead>{this.state.counter}</StateHead>{' '}
        </Div_Head>
        <Div_Buttons>
          <ButtonIncrement
            onClick={() => {
              this.setState({
                counter: this.state.counter + 1,
              })
            }}
          >
            increment
          </ButtonIncrement>

          <ButtonDecrement
            onClick={() => {
              this.setState({
                counter: this.state.counter - 1,
              })
            }}
          >
            decrement
          </ButtonDecrement>
        </Div_Buttons>
      </>
    )
  }
}

const Header_Main_H1 = styled.h1`
  text-align: center;
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  background: ${colors.header};
  width: 500px;
  justify-content: center;
`

const ButtonIncrement = styled(Button)`
  &:hover {
    background-color: ${colors.lightGreen};
  }
`

const ButtonDecrement = styled(Button)`
  &:hover {
    background-color: ${colors.pinkRed};
  }
`
const StateHead = styled.h1`
  padding: 15px 32px;
  text-align: center;
  background-color: ${colors.header};
  border-radius: 10px;
  border: 1px solid ${colors.grey};
  width: 500px;
  justify-content: center;

  @media (max-width: ${size.mobile}) {
    flex-direction: column;
    width: 100%;
    height: 20%;
    padding-top: 10%;
    top: 0;
    left: 0;
  }
`
const Div_Head = styled.div`
  justify-content: center;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
`
const Div_Buttons = styled(Div_Head)`
  flex-direction: row;
`
