import styled, { createGlobalStyle } from 'styled-components'

export const colors = {
  white: '#ffffff',
  black: '#111111',
  hslaColor9: 'hsl(192, 100%, 9%)',
  rgbaBlack2: 'rgba(0, 0, 0, 0.2)',
  rgbaBlack5: 'rgba(0, 0, 0, 0.5)',
  rgbaBlack7: 'rgba(0, 0, 0, 0.7)',
  rgbaBlack15: 'rgba(0, 0, 0, 0.15)',
  rgbaBlack037: 'rgba(0, 0, 0, 0.37)',
  rgbaBlack0: 'rgba(255, 255, 255, 0)',
  lightGreen: '#4caf50',
  blue: '#008CBA',
  trueBlue: '#2176ff',
  turquoise: '#26b3a3',
  lightBlue: '#61dafb',
  pinkRed: '#f1356d',
  purple: '#2e1566',
  blackGrey: '#222',
  jsYellow: '#FFC93F',
  grey: '#b8b8b8',
  rgbaGrey01: 'rgba(255, 255, 255, 0.1)',
  rgbaGrey018: 'rgba(255, 255, 255, 0.18)',
  react: '#61DAFB',
  header: '#ebfbff',
  body: '#fff',
  brown: '#333',
}

export const size = {
  big: '30px',
  normal: '18px',
  small: '14px',
  mobile: '768px',
  media800: '800px',
}

export const font = {
  serif: 'serif',
  courierNew: 'Courier New',
  gillSans: 'gill sans',
  popins: 'Poppins',
}

export const GlobalStyles = createGlobalStyle`
     @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&display=swap');

    box-sizing: border-box;
    background: white;
    color: ${colors.hslaColor9};
    font-family: 'Poppins', sans-serif;
    font-size: 1.15em;
    margin: 0;
    opacity: 0.6;
    line-height: 1.5;
   
`
