import { BlurryWindow } from '../components/BlurryWindow'
import { Head } from './components/Head'
import { Page } from './components/Page'
import { colors } from '../theme'
import { size } from '../theme'
import styled from 'styled-components'

export const Who = () => {
  return (
    <Page>
      <WhoHead>A Brief History of JavaScript</WhoHead>
      <BlurryWindow>
        JavaScript is arguably the most important programming language in the world. Google,
        YouTube, Wikipedia, Yahoo!, Amazon, Facebook, eBay, LinkedIn, and Twitter have all been
        built using JavaScript. Virtually everything a user directly interacts with on these
        websites has been made with JavaScript. There’s no question about it: becoming a JavaScript
        programmer is one of the best career choices you can possibly make in 2020. So how do you
        start learning it in San Diego? Are there any other languages that go well with it? What are
        the Ruby on Rails vs JavaScript pros and cons? Should JavaScript be used together with Ruby?
        Before you truly dive into the technical side of coding, it’s important to get an
        understanding of how JavaScript was first introduced, and how it became the programming
        powerhouse it is today. Read on to learn more.
      </BlurryWindow>
    </Page>
  )
}
const WhoHead = styled(Head)`
  font-size: ${size.big};
  color: ${colors.black};
`
