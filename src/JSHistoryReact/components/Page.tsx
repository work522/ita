import styled from 'styled-components'

export const Page = styled.section`
  padding: 20px;
  text-align: center;
  margin: 20px auto;
  display: flex;
  flex-direction: column;
  align-items: center;
`
