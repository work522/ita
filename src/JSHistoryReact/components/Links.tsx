import styled from 'styled-components'

export const Links = styled.p`
  text-align: center;

  margin: 15px auto;
  padding: 15px;
`
