import { NavLink as Link } from 'react-router-dom'
import { colors, size } from '../../theme'
import styled from 'styled-components'

export const Nav = styled.nav`
  background: #fff;
  height: 100px;
  display: flex;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  text-align: center;
  align-items: center;
  border-bottom-width: medium;
  border-radius: 20px;
  margin: 30px;
  font-size: ${size.normal};
  @media (max-width: ${size.mobile}) {
    flex-direction: column;
    width: 100%;
    height: 20%;
    padding-top: 10%;
    top: 0;
    left: 0;
  }
`

export const NavLink = styled(Link)`
  color: ${colors.black};
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;

  &.active {
    color: ${colors.blue};
  }
`

export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  background: ${colors.header};
  padding: 20px;
  border-radius: 50px;
  height: 50%;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};

  @media screen and (max-width: ${size.mobile}) {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    height: 20%;
    padding-top: 0%;
    margin-bottom: 7%;
    top: 0;
    left: 0;
  }
`
