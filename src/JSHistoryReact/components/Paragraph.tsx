import styled from 'styled-components'

export const Paragraph = styled.div`
  max-width: 600px;
  padding: 0px;
  text-align: center;
  margin: 20px auto;
`
