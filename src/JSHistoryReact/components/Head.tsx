import styled from 'styled-components'

export const Head = styled.h1`
  text-align: center;
  max-width: 600px;
  margin: 20px auto;
  padding: 20px;
`
