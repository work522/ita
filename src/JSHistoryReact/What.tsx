import { BlurryWindow } from '../components/BlurryWindow'
import { Head } from './components/Head'
import { Page } from './components/Page'
import { colors } from '../theme'
import { size } from '../theme'
import styled from 'styled-components'

export const What = () => {
  return (
    <Page>
      <WhatHead>What was JavaScript originally called?</WhatHead>
      <BlurryWindow>
        The early versions of JavaScript were called Mocha. Not long after a Mocha prototype was
        introduced into Netscape Communicator (May 1995), it was renamed to LiveScript, purely
        because the world Live was better for marketing. It was renamed again in December of the
        same year, this time into JavaScript. This first version of JavaScript defined the many
        great traits (such as its object-model) this coding language is known for today. It also
        boasted a number of powerful features that would eventually enable it to outgrow its
        original purpose.
      </BlurryWindow>
    </Page>
  )
}

const WhatHead = styled(Head)`
  font-size: ${size.big};
  color: ${colors.black};
`
