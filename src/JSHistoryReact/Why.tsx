import { BlurryWindow } from '../components/BlurryWindow'
import { Head } from './components/Head'
import { Page } from './components/Page'
import { colors } from '../theme'
import { size } from '../theme'
import styled from 'styled-components'

export const Why = () => {
  return (
    <Page>
      <WhyHead>Why was JavaScript created?</WhyHead>
      <BlurryWindow>
        Back in 1995, Netscape Communicator (a paid internet browser) was by far the most popular
        web browser in the world. The founder of Netscape Communications, Marc Andreeseen, wanted to
        make the web more dynamic by making animations, user interaction, and other types of
        automation a standard part of any website. He also knew that Microsoft was hard at work on
        their own browser, Internet Explorer, and wanted to make Netscape Communicator more
        attractive to developers by equipping it with both an enterprise-level coding language
        (Java) and a smaller scripting language (JavaScript). While this plan didn’t stop Microsoft
        from taking over the web browser market and driving Netscape Communications out of business,
        JavaScript nonetheless managed to escape its “walled garden” roots.
      </BlurryWindow>
    </Page>
  )
}
const WhyHead = styled(Head)`
  font-size: ${size.big};
  color: ${colors.black};
`
