import { Nav, NavLink, NavMenu } from './components/NavbarElemenets'

import { urls } from '../urls'

export const Navbar = () => {
  return (
    <>
      <Nav>
        <NavMenu>
          <NavLink to={urls.divider}>Home</NavLink>

          <NavLink to={urls.mainPageJSHistory}>Main Page</NavLink>

          <NavLink to={urls.who}>Who introduce JavaScript?</NavLink>

          <NavLink to={urls.what}>What was JavaScript originally called?</NavLink>

          <NavLink to={urls.why}>Why was JavaScript created?</NavLink>
        </NavMenu>
      </Nav>
    </>
  )
}
