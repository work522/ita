import { MainPageHw2 } from './MainPageHw2'
import { Navbar } from './Navbar'
import { Route, Routes } from 'react-router-dom'
import { What } from './What'
import { Who } from './Who'
import { Why } from './Why'
import { urls } from '../urls'

export const JSHistoryReact = () => {
  return (
    <div className='App'>
      <Navbar />
      <div className='content'>
        <Routes>
          <Route path={urls.mainPageJSHistory} element={<MainPageHw2 />} />

          <Route path={urls.what} element={<What />} />

          <Route path={urls.who} element={<Who />} />

          <Route path={urls.why} element={<Why />} />
        </Routes>
      </div>
    </div>
  )
}
