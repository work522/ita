export const urls = {
  divider: '/',
  what: 'what',
  why: 'why',
  who: 'who',
  mainPageJSHistory: 'main-page-hw-2',
  mainPageCounter: 'main-page-hw-3',
  mainPageToDoList: 'main-page-hw-4',
}
