import { BlurryWindow } from '../components/BlurryWindow'
import { Checkbox } from './components/Checkbox'
import { DeleteIcon } from './components/DeleteIcon'
import { TextButton } from './components/TextButton'
import { colors, font, size } from '../theme'
import { nanoid } from 'nanoid'
import { useLocalStorage } from './components/useLocalStorage'
import { useState } from 'react'
import styled from 'styled-components'

const H1_Main = styled.h1`
  align-items: center;
  background: ${colors.header};
  color: ${colors.black};
  display: flex;
  height: auto;
  justify-content: center;
  text-decoration: none;
  width: auto;
  margin-bottom: 45px;
  padding: 15px 60px;
  font-family: ${font.popins};
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
`

const Layout_Div_Main = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 35px;
`

const Div_Main_Container = styled.div`
  display: flex;
  flex: 0 1 460px;
  flex-direction: column;
  align-items: stretch;
`
const P_UserInput = styled.input`
  background: ${colors.header};
  border-radius: 15px;
  color: ${colors.black};
  width: 50%;
  height: 50%;
  margin: 15px;
  padding: 20px;
  border-style: solid;
  border-color: ${colors.grey};
  font-family: ${font.popins};
  font-size: 18px;
  @media (max-width: 800px) {
    width: 50%;
    height: 10%;
    padding-top: 30%;
    top: 0;
    left: 0;
  }
`
const Div_NameTasks = styled.label`
  margin: 13px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: ${font.popins};
  font-size: ${size.normal};
`
const Div_Error = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;
  margin-right: auto;
  font-family: ${font.popins};
  font-weight: bold;
  color: ${colors.black};
`
const IconButton = styled.button`
  background: none;
  border: none;
`
const Div_FilterStyled = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

type Task = {
  id: string
  name: string
  complete: boolean
}
export const ToDoList = () => {
  const [tasks, setTasks] = useLocalStorage('tasks', [] as Task[])
  const [name, setName] = useState('')
  const [error, setError] = useState(null as string | null)
  const [filter, setFilter] = useLocalStorage('Complete', 'Active')

  const filterTasks = () => {
    if (filter === 'Active') {
      return tasks.filter(task => !task.complete)
    } else if (filter === 'Complete') {
      return tasks.filter(task => task.complete)
    } else {
      return tasks
    }
  }

  const handleNewTaskClick = () => {
    if (name.trim().length === 0) {
      setError("You can't add empty task!")
      return
    }
    setName('')
    setError('')
    setTasks([...tasks, { id: nanoid(), name: name, complete: false }])
  }

  const handleChecked = (used: Task) => {
    setTasks(
      tasks.map(task => (task.id === used.id ? { ...task, complete: !used.complete } : task))
    )
  }

  const handleDelete = (used: Task) => setTasks(tasks.filter(task => task.id !== used.id))

  const handleClear = () => setTasks(tasks.filter(task => !task.complete))

  return (
    <Div_Main_Container>
      <Layout_Div_Main>
        <H1_Main>Todo List</H1_Main>
        <P_UserInput type='text' value={name} onChange={e => setName(e.target.value)} />
        {error && <Div_Error>{error}</Div_Error>}
        <TextButton onClick={handleNewTaskClick}>Add new task</TextButton>
        <BlurryWindow>
          {filterTasks().map(task => (
            <Div_NameTasks key={task.id}>
              <Checkbox checked={task.complete} onChange={() => handleChecked(task)} />
              {task.name}
              <IconButton onClick={() => handleDelete(task)}>
                <DeleteIcon />
              </IconButton>
            </Div_NameTasks>
          ))}
        </BlurryWindow>
        <Div_FilterStyled>
          <TextButton onClick={() => setFilter('Active')}>Active</TextButton>
          <TextButton onClick={() => setFilter('Complete')}>Completed</TextButton>
          <TextButton onClick={() => setFilter('All')}>All</TextButton>
        </Div_FilterStyled>
        <TextButton onClick={handleClear}>Clear tasks</TextButton>
      </Layout_Div_Main>
    </Div_Main_Container>
  )
}
