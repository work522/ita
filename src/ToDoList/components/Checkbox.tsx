import { ComponentProps } from 'react'
import { colors } from '../../theme'
import styled, { StyledComponent } from 'styled-components'

const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
`
const HiddenCheckbox = styled.input`
  // Hide checkbox visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`
const StyledCheckbox = styled.div<{ checked: boolean }>`
  display: inline-block;
  width: 16px;
  height: 16px;
  background: ${props => (props.checked ? colors.react : colors.black)};
  border-radius: 3px;
  transition: all 150ms;
  margin-right: 16px;
  display: flex;
  border-radius: 100%;
  cursor: pointer;
`
type Props = ComponentProps<StyledComponent<'input', any, {}>> & {
  checked: boolean
}

export const Checkbox = ({ checked, ...props }: Props) => (
  <CheckboxContainer>
    <HiddenCheckbox {...props} type='checkbox' checked={checked} />
    <StyledCheckbox checked={checked} />
  </CheckboxContainer>
)
