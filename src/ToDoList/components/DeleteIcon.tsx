import { colors } from '../../theme'
import styled from 'styled-components'
import trash from '../../icons/trash.svg'

const IconButton = styled.button`
  background: ${colors.white};
  border: none;
  cursor: pointer;
`

export const DeleteIcon = () => {
  return <img src={trash} alt='trash' />
}
