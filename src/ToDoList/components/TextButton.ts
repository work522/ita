import { colors, font, size } from '../../theme'
import styled from 'styled-components'

export const TextButton = styled.button`
  background: ${colors.header};
  color: ${colors.black};
  font-size: ${size.small};
  align-self: center;
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  border-radius: 15px;
  padding: 20px;
  margin: 30px;
  font-family: ${font.popins};
  user-select: none;
  &:hover {
    transform: scale(1.1);
  }
`
