import { colors, font } from '../theme'
import styled from 'styled-components'

export const BlurryWindow = styled.div`
  width: 460px;
  max-width: 100%;
  padding: 15px;
  background: ${colors.black};
  background: linear-gradient(135deg, ${colors.rgbaGrey01}, ${colors.rgbaBlack0});
  -webkit-backdrop-filter: blur(20px);
  backdrop-filter: blur(20px);
  box-shadow: 0 8px 32px 0 ${colors.rgbaBlack037};
  border: 1px ${colors.rgbaGrey018};
  border-radius: 32px;
  font-family: ${font.popins};
`
