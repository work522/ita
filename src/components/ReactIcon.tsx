import react from '../react.svg'
import styled from 'styled-components'

const Div_ReactIcon = styled.div`
  background: none;
  border: none;
  width: 20%;
`

export const ReactIcon = () => {
  return (
    <Div_ReactIcon>
      <img src={react} alt='react' />
    </Div_ReactIcon>
  )
}
