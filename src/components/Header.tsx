import { Div_Container1200 } from './styles/Container'
import { Flex } from './styles/Flex'
import { ReactIcon } from './ReactIcon'

import { colors } from '../theme'
import styled from 'styled-components'

export const Header = () => {
  return (
    <Div_StyledHeader>
      <Div_Container1200>
        <Flex>
          <div>
            <h1>Welcome on my Website!</h1>
            <p>Road to the Front-End developer</p>
          </div>
          <ReactIcon />
        </Flex>
      </Div_Container1200>
    </Div_StyledHeader>
  )
}

const Div_StyledHeader = styled.header`
  background: ${colors.header};
  padding: 10px 0;
  width: 100%;
  margin-bottom: 20px;
`
