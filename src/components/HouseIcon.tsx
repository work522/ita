import house from '../icons/house.svg'
import styled from 'styled-components'

const IconButton = styled.button`
  background: white;
  border: none;
  cursor: pointer;
`
export const HouseIcon = () => {
  return (
    <IconButton>
      <img src={house} alt='house' />
    </IconButton>
  )
}
