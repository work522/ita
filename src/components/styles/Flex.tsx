import { size } from '../../theme'
import styled from 'styled-components'

export const Flex = styled.div`
  display: flex;
  align-items: center;
  display: flex;
  justify-content: space-between;

  @media (max-width: ${size.mobile}) {
    flex-direction: column;
    text-align: center;
  }
`
