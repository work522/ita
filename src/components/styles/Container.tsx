import styled from 'styled-components'

export const Container = styled.div`
  width: auto;
  max-width: 100%;
  padding: 0 20px;
  margin: 0 20px;
`
export const Div_Container1200 = styled(Container)`
  max-width: 1200px;
  margin: 0 auto;
`
