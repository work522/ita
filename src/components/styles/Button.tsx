import { colors } from '../../theme'
import styled from 'styled-components'

export const Button = styled.button`
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  padding: 15px 60px;
  background-color: ${colors.white};
  color: ${colors.brown};

  &:hover {
    opacity: 0.9;
    transform: scale(0.98);
  }
`
