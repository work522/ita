import { colors } from '../theme'
import React, { useState } from 'react'
import styled from 'styled-components'

export const Popup = () => {
  const [popup, setPopup] = useState(false)

  const togglePopup = () => {
    setPopup(!popup)
  }

  return (
    <>
      <Btn_PopupOpenBtn onClick={togglePopup} className='btn-popup'>
        About me
      </Btn_PopupOpenBtn>

      {popup && (
        <Div_Popup className='popup'>
          <Div_PopupText>
            <Div_Overlay onClick={togglePopup} className='overlay'></Div_Overlay>
            <h2>Hi I am Patrik :)</h2>
            <p>
              I am trying to move from industry to IT,
              <br /> that is why I started College as a software developer.
              <br /> I am programming in JavaScript at school - backend and frontend,
              <br /> but in my free time I focus on the frontend, specifically React,
              <br /> and I am slowly learning TypeScript.
              <br /> Right now I work as a PLC software developer.
              <br /> I have no hobbies because I am overloaded by College and programming. :)
              <br /> But when I do have time I like hiking and climbing, and ride a bike,
              <br /> somewhere in the forest.
            </p>
            <Btn_PopupCloseBtn className='close-popup' onClick={togglePopup}>
              CLOSE
            </Btn_PopupCloseBtn>
          </Div_PopupText>
        </Div_Popup>
      )}
    </>
  )
}

const Div_Popup = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: 0%;
  padding: 0%;
  width: 100%;
  height: 100%;
  background-color: ${colors.rgbaBlack2};
  color: black;
  overflow-y: hidden;
  display: inline-block;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  text-align: center;
  border-radius: 20px;
  position: fixed;
  z-index: 20;

  @media (max-width: 800px) {
    flex-direction: column;
    width: 100%;
    height: 200%;
    .active-popup {
      overflow-y: hidden;
    }
  }
`
const Div_Overlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: ${colors.rgbaBlack7};
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
`
const Btn_PopupOpenBtn = styled.button`
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  padding: 15px 60px;
  margin: 0 0 60px 0;
  background-color: ${colors.white};
  color: ${colors.brown};

  &:hover {
    opacity: 0.9;
    transform: scale(0.98);
  }
`

const Btn_PopupCloseBtn = styled.button`
  border-radius: 50px;
  border: none;
  box-shadow: 0 0 10px ${colors.rgbaBlack15};
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  padding: 15px 60px;
  background-color: ${colors.white};
  &:hover {
    opacity: 0.9;
    transform: scale(0.98);
  }
`
const Div_PopupText = styled.div`
  background: ${colors.white};
  max-width: 600px;
  margin: 100px auto 0 auto;
  padding: 20px;
  word-break: break-all;
`
