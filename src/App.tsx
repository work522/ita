import 'normalize.css'
import { Container } from './components/styles/Container'
import { Counter } from './Counter/Counter'
import { Divider } from './Divider'

import { GlobalStyles } from './theme'
import { JSHistoryReact } from './JSHistoryReact/JSHistory'
import { Route, Routes } from 'react-router-dom'
import { ToDoList } from './ToDoList/ToDoList'
import { urls } from './urls'

export const App = () => {
  return (
    <>
      <GlobalStyles />
      <link
        rel='stylesheet'
        href='https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css'
      />
      <Routes>
        <Route path={urls.divider} element={<Divider />} />

        <Route path={'/*'} element={<JSHistoryReact />} />

        <Route path={urls.mainPageCounter} element={<Counter />} />

        <Route path={urls.mainPageToDoList} element={<ToDoList />} />
      </Routes>
    </>
  )
}
